#--------------------------------------------------------#
#-----------------DEFINING MODULES-----------------------#
#--------------------------------------------------------# 
import socket
import os
import select
import sys
import subprocess
import time
import random
os.system("clear")
BUFF = 1026							# DEFINING BUFFER FOR SOCKET
BUFF1 = 256

ipcount = 0							# TO CALCULATE DEGREE OF NODE

#---------------------------------------------------------#
#-----------------JOIN FUNCTION DEFINATION----------------#
#---------------------------------------------------------#
def JOIN(NODE_IP, NODE_PORT):					# DEFINATION OF JOIN COMMAND	
	
	NODEIP = NODE_IP
	NODEPORT = NODE_PORT
	NODEADDR = (str(NODEIP), int(NODEPORT))			# ADDRESS OF NODE PORT RECEIVED FROM BOOTSTRAP
	join = "JOIN"+" "+str(MY_HOSTADDR)+" "+str(MY_PORT)
	r = int(len(join))
	s = r + 4
	MSG_JOIN = "00"+str(s)+" "+str(join)			# MESSAGE FOR JOINING NODE
	print "SENDING JOIN COMMAND TO: "+str(NODEADDR)
	print str(MSG_JOIN)
	sok.sendto(MSG_JOIN, NODEADDR)				# SENDING JOIN MESSAGE
	ROUTEVAL = ROUTING(NODEIP, NODEPORT)			# UPDATING ROUTING TABLE
	return 0
#--------------END OF JOIN COMMAND DEFINATION------------#

#--------------------------------------------------------#
#--------------DEFINATION OF JOINOK VALUE----------------#
#--------------------------------------------------------#
def JOINOK(ADDR):						# DEFING JOINOK FUNCTION
	print "NODES JOINED SUCCESSFULLY..!!"			
	msg_joinok = "JOINOK"+" "+"0"
	r = int(len(msg_joinok))
	s = r + 4
	MSG_JOINOK = "00"+str(s)+" "+str(msg_joinok)		# DEFINING JOINOK MESSAGE
	sok.sendto(MSG_JOINOK, ADDR)				# SENDING JOINOK TO ADDRESS
	
#-----------------UNREGISTER COMMAND DEFINATION-----------#
def UNREG(NODE_IP, NODE_PORT, NODE_USERNAME):
	
	msg_unreg = "UNREG"+" "+str(NODE_IP)+" "+str(NODE_PORT)+" "+str(NODE_USERNAME)
	r = int(len(msg_unreg))
	s = r + 4
	MSG_UNREG = "00"+str(s)+" "+str(msg_unreg)		# MESSAGE FOR UNREGISTERING
	print str(MSG_UNREG)
	BS_ADDR = (str(HOST), int(PORT))
	sok.sendto(MSG_UNREG, BS_ADDR)				# SENDING UNREGISTER COMMAND
	#time.sleep(10)
	MSGRECV_UNREG, MSRECV_ADDR = sok.recvfrom(BUFF1)		# RECEIVING AFTER UNREGISTER COMMAND
	print "MESSAGE RECEIVED AFTER UNREGISTERING \n"
	print str(MSGRECV_UNREG)
	return str(MSGRECV_UNREG)				# SEND THE MESSAGE RECEIVED
#-------------END OF UNREGISTER COMMAND DEFINATION-------#
	
#-----------DEFINATION OF UNREGISTER COMMAND---------#
def UNREGOK(MESSAGE):						# DEFINING UNREGISTER FUNCTION
	F4 = open("UNREGISTER.txt", "wb")
	F4.write(str(MESSAGE))					
	F4.close()
	F5 = open("UNREGISTER.txt", "r+")
	WORDS1 =[]
	contents = F5.readlines()
	for i in range(len(contents)):
        	WORDS1.extend(contents[i].split())
        F5.close()
	if (WORDS1[2] == 0):
		return 0
	else:
		return 1
#-----------END OF DEFINATION OF UNREGISTER COMMAND-----#

#-----------DEFINATION OF LEAVE COMMAND-----------------#
def LEAVE(NODE_IP, NODE_PORT, NODE_USERNAME):			# DEFINING LEAVE FUNCTION
	F13 = open("ROUTING_TABLE.txt", "r+")
	content = F13.readlines()
	delimiters = ["\n", " "]
	words = content
	for delimiter in delimiters:
		new_words = []
		for word in words:
        		new_words += word.split(delimiter)
    			words = new_words			# CONTAINS IP ADDRESS, PORT NOS 

	LEN = len(new_words)
	count = 0
	while (count < LEN):
		IP = str(new_words[count])
		count = count + 1
		PORT = int(new_words[count])
		count = count + 2
		levmsg = "LEAVE"+" "+str(MY_HOSTADDR)+" "+str(MY_PORT)
		r = int(len(levmsg))
		s = r + 4
		LEVMSG = "00"+str(s)+" "+str(levmsg)			# DEFINING LEAVE MESSAGE
		SERADDR = (str(IP), int(PORT))				# ADDRESS TO SEND LEAVE COMMAND
		print "SENDING LEAVE COMMAND..!!"+str(SERADDR)
		print str(LEVMSG)
		sok.sendto(LEVMSG, SERADDR)				# SENDING LEAVE COMMAND
			
	unreg_msg = "UNREG"+" "+str(MY_HOSTADDR)+" "+str(MY_PORT)+" "+str(MY_USR)
	r = len(str(unreg_msg))
	s = r + 4
	UNREG_MSG = "00"+str(s)+" "+str(unreg_msg)			# DEFINING UNREGISTER MESSAGE
	sok.sendto(UNREG_MSG, BS_ADDR)					# SENDING UNREGISTER COMMAND TO BOOTSTRAP
	BSUNREG_MSG, UNREG_ADDR = sok.recvfrom(BUFF1)
	print "BOOTSTRAP MESSAGE FROM: "+str(UNREG_ADDR)+" "+str(BSUNREG_MSG)
	sok.close()					# CLOSING SOCKET DEFINATION
	sys.exit(0)
	return 0					# SEND THE MESSAGE RECEIVED
	
#--------END OF DEFINATION OF LEAVE COMMAND--------#

#--------------------------------------------------#
#---------------ROUTING TABLE DEFINATION-----------#
#--------------------------------------------------#
def ROUTING(NODE_IP, NODE_PORT):			# DEFINATION OF ROUTING TABLE
	F3 = open("ROUTING_TABLE.txt", "a")
	nl = "\n"
	DETAIL = str(NODE_IP)+" "+str(NODE_PORT)	# MAKING LINE OF DETAILS
	LINE = DETAIL, nl
	F3.writelines(LINE)				# WRITING THE DETAIL IN THE ROUTING TABLE
	F3.close()
	
	F8 = open("ROUTING_TABLE.txt", "r+")		# OPENING ROUTING TABLE IN READ MODE
	DETAILS = F8.readlines()			# READING FROM ROUTING TABLE
	print "ROUTING TABLE UPDATED..!!"
	print str(DETAILS)				# DISPLAYING ROUTING TABLE
	F8.close()
	return 0
#-----------END OF ROUTING TABLE DEFINATION--------------#

#------------------------------------------------------#
#-------------DEFINATION OF SEARCH COMMAND-------------#
#------------------------------------------------------#
def SEARCH(QUERY, HOPCOUNT):
	F12 = open("ROUTING_TABLE.txt", "r+")
	content = F12.readlines()
	delimiters = ["\n", " "]
	words = content
	for delimiter in delimiters:			# READING THE CONTENTS OF ROUTING TABLE
		new_words = []
		for word in words:
        		new_words += word.split(delimiter)
    			words = new_words		

	LEN = len(new_words)
	count = 0
	if (str(HOPCOUNT) <= "5"):
		while(count < LEN):
			IP = str(new_words[count])	# SELECTING IP ADDRESS
			count = count + 1
			PORT = int(new_words[count])	# SELECTING PORT
			sermsg = "SER"+" "+str(SERNODEIP)+" "+str(SERNODEPORT)+" "+"+"+str(QUERY)+"+"+str(HOPCOUNT)# DEFINING SEARCH MESSAGE
			r = int(len(sermsg))
			s = r + 4
			SERMSG ="00"+str(s)+" "+str(sermsg)
			SERADDR = (str(IP), int(PORT))	# DEFINING SEARCH ADDRESS
			print "SENDING SEARCH COMMAND TO NODES.."+str(SERADDR)
			print (str(SERMSG))
			sok.sendto(SERMSG, SERADDR)	# SENDING SEARCH COMMAND TO THE ADDRESS
			count = count + 2	
	else:
		print "HOP COUNT EXCEEDED..!!"
		#LEVMSG = LEAVE(MY_HOSTADDR, MY_PORT, MY_USR);

	return (0)
#--------END OF DEFINATION OF SEARCH COMMAND--------------#

#----------------------------------------------------------#
#-------DEFINATION OF SEARCHING IN OWN FILE LIST-----------#
#----------------------------------------------------------#
def SEARCHOWN(FILE):
	f1 = open("myfile.txt", "r+")				# OPENING MYFILE.TXT
	str3 = f1.readlines()					# READING FROM MYFILE.TXT
	count = 0
	j = 0
	for j in range(len(str3)):
		x = int(len(str3[j]))
		y = int(len(FILE))
		if (y == (x+1)):
			count = count + 1			# INCREASING COUNT VALUE FOR THE FILE
			j = j + 2
		else:
			j = j + 2
		
	f1.close()
	return (str(count))					# RETURNING NOS OF FILES PRESENT IN THE CURRENT NODE
#------END OF DEFINATION OF SEARCH IN OWN FILE LIST-------#

#---------------------------------------------------------#
#-------------------START OF MAIN-------------------------#
#---------------------------------------------------------# 

MSG = sys.argv[3]+" "+sys.argv[4]+" "+sys.argv[5]+" "+sys.argv[6]+" "+sys.argv[7]	# USER INPUT FOR REGISTRATION
print "COMMAND ENTERED: "+str(MSG)
BSREQ_MSG = str(MSG)

sok = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)	# CREATING A UDP SOCKET

HOST = socket.gethostbyname(sys.argv[1])			# SELECTING BOOTSTRAP HOST
PORT = sys.argv[2]					# SELECTING BOOTSTRAP PORT
BS_ADDR = (str(HOST), int(PORT))			# DEFINING BOOTSTRAP ADDRESS
MY_HOSTADDR = sys.argv[5]				# SELECTING MY HOST
MY_PORT = sys.argv[6]					# SELECTING MY PORT
MY_USR = sys.argv[7]					# SELECTING MY USER

MY_ADDR = (str(MY_HOSTADDR), int(MY_PORT))		# DEFINING MY ADDRESS
sok.bind(MY_ADDR)					# BINDING THE SOCKET TO ADDRESS

sok.sendto(BSREQ_MSG, BS_ADDR)				# SENDING MESSAGE TO BOOTSTRAP
BSREC_MSG, ADDR = sok.recvfrom(BUFF1)			# RECEIVING MESSAGE FROM BOOTSTRAP
print "MESSAGE RECEIVED FROM BOOTSTRAP: " +str(BSREC_MSG)		# PRINTING RECEIVED MESSAGE FROM BOOTSTRAP
#------------END OF BOOTSTRAP FUNCTION------------------#


#------------READING AND WRITING THE BS COMMAND---------#
F1 = open("REG.txt", "wb")				# WRITING THE BS MESSAGE IN A FILE
F1.write(str(BSREC_MSG))
F1.close()						# CLOSING FILE DESCRIPTOR

F2 = open("REG.txt", "r+")
WORDS =[]
contents = F2.readlines()
for i in range(len(contents)):
         WORDS.extend(contents[i].split())

#-----------------------------------------------------#
#-----------ANALYSIS OF BOOTSTRAP REPLY---------------#
#-----------------------------------------------------#
COND = int(WORDS[2])				# BOOTSTRAP CONDITION
if (COND == 0):
	print "YOU ARE THE FIRST NODE TO JOIN..!!"
	ipcount = 0
	
elif (COND == 1):					# ONLY ONE NODE IS PRESENT
	NEWNODE_IP = WORDS[3]				# DECLARING THE NODEIP, NODEPORT
	NEWNODE_PORT = WORDS[4]
	JOIN_RETURN = JOIN(NEWNODE_IP, NEWNODE_PORT);	# CALLING THE JOIN COMMAND TO SEND JOIN REQUEST
	ipcount = ipcount + 1
	print "DEGREE OF THIS NODE IS: "+str(ipcount)	# DEGREE OF NODE
	if (JOIN_RETURN == 0):
		print "NODE ACCEPTED JOIN COMMAND..!! ROUTING TABLE UPDATED..!!"
	else:
		print "UNSUCCESSFUL..!!"
	
elif (COND == 2):					# TWO NODES ARE PRESENT
	NEWNODE1_IP = WORDS[3]				# DECLARING FIRST NODEIP, NODEPORT
	NEWNODE1_PORT = WORDS[4]
	ipcount = ipcount + 2
	print "DEGREE OF THIS NODE IS: "+str(ipcount)	# DEGREE OF NODE
	JOIN_RETURN = JOIN(NEWNODE1_IP, NEWNODE1_PORT);	# CALLING JOIN FUNCTION TO SEND JOIN REQUEST
	if (JOIN_RETURN == 0):
		print "NODE ACCEPTED JOIN COMMAND..!! ROUTING TABLE UPDATED..!!"
	else:
		print "UNSUCCESSFUL..!!"
	NEWNODE2_IP = WORDS[5]				# DECLARING SECOND NODEIP, NODEPORT
	NEWNODE2_PORT = WORDS[6]
	JOIN_RETURN = JOIN(NEWNODE2_IP, NEWNODE2_PORT);	# CALLING JOIN COMMAND TO SEND JOIN REQUEST
	if (JOIN_RETURN == 0):
		print "NODE ACCEPTED JOIN COMMAND..!! ROUTING TABLE UPDATED..!!"
	else:
		print "UNSUCCESSFUL..!!"
	
elif (COND == 9999):					# ERROR WHILE REGISTERING IN BOOTSTRAP
	print "ERROR IN COMMAND..!!"
	sys.exit(0)					# EXIT FROM SYSTEM, REGISTER AGAIN
elif (COND == 9998):					# ALREADY REGISTERED, NEED TO UNREGISTER
	print "ALREADY REGISTERED, UNREGISTER FIRST..!!"
	UNREG_RETRN = UNREG(MY_HOSTADDR, MY_PORT, MY_USR);	# CALLING UNREGISTER FUNCTION TO UNREGISTER
	UNREGOK_RETRN = UNREGOK(UNREG_RETRN);		# CALLING UNREGISTER OK FUNCTION
	if (UNREGOK_RETRN == 0):
		sys.exit(0)
	else:
		print "HOST"+" "+str(MY_ADDR)+" "+"UNREGISTERED SUCCESSFULLY..!!"
		sys.exit(0)
elif (COND == 9997):
	print "REGISTER WITH DIFFERENT NODE IP AND PORT NOS.."
	sys.exit(0)
else:
	print "BOOTSTRAP FULL..!! CAN'T REGISTER..!!"
	sys.exit(0)
#-------------END OF BOOTSTRAP COMMAND DEFINATION-------#

#------------------------------------------------------#
#--------------CREATING "MY FILE.txt"------------------#
#------------------------------------------------------#
f1 = open("filename.txt", "r+")
f3 = open("myfile.txt", "a")

str1 = f1.readlines()							# READING FROM FILENAME.TXT 
n = int(raw_input("ENTER THE NOS OF FILE TO BE SELECTED (max: 5)"))	# USER INPUT TO SELECT HOW MANY FILES TO BE SELECTED
count = 0

while (count != n):
	y = random.randrange(1,n)					# RANDOMLY SELECTING FILES FROM FILENAME.TXT
	file1 = str1[y]
	f3.writelines(str(file1))					# WRITING IN MYFILE.TXT
	count = count + 1

f3.close()
f3 = open("myfile.txt", "r+")
str1 = f3.readlines()
print "CONTENTS OF FILE AT THIS NODE: "
print str(str1) 							# READING THE CONTENTS OF MYFILE.TXT
sermsg = 0								# TO COUNT NOS OF SEARCH MESSAGE

FILES = [" "]
#----------------------------------------------------------------#
#---------------------- START OF SERVER MODE --------------------#
#----------------------------------------------------------------#
while(1):
	
	MY_ADDR = (str(MY_HOSTADDR), int(MY_PORT))
	MSSG_NODE, ADDR_NODE = sok.recvfrom(BUFF)	# MAKING THE NODE LISTEN FOR INCOMING CONNECTIONS
	F6 = open("REQUEST.txt", "wb")
	F6.writelines(str(MSSG_NODE))
	F6.close()
	
	F7 = open("REQUEST.txt", "r+")
	WORDS2 =[]
	contents = F7.readlines()
	for i in range(len(contents)):
        	WORDS2.extend(contents[i].split())
        
        if (WORDS2[1] == "JOIN"):				# FOR JOIN REQUEST
        	print "RECEIVED JOIN COMMAND FROM: "+str(ADDR_NODE)
        	ipcount = ipcount + 1
        	print "DEGREE OF THIS NODE: "+str(ipcount)	# DEGREE OF NODE
        	print str(MSSG_NODE)
        	NEWNODE1_IP = WORDS2[2]				# INCOMING NODE ADDRESS
        	NEWNODE1_PORT = WORDS2[3]			# INCOMING NODE PORT
        	NEWNODE1_ADDR = (str(NEWNODE1_IP), int(NEWNODE1_PORT))	
        	JOINOK_RET = JOINOK(ADDR_NODE)			# CALLING JOINOK FUNCTION
        	ROUTVAL = ROUTING(NEWNODE1_IP, NEWNODE1_PORT)	# FOR UPDATION OF ROUTING TABLE
        	
        elif (WORDS2[1] == "SER"):				# FOR SEARCH REQUEST
        	sermsg = sermsg + 1
        	print "SEARCH MESSAGE TILL NOW IS: "+str(sermsg)	# DISPLAY NOS OF SEARCH MESSAGE RECEIVED
		print "SEARCH COMMAND RECEIVED: "+str(ADDR_NODE)
		print str(MSSG_NODE)				# PRINTING THE SEARCH COMMAND
		MSSGG = MSSG_NODE.split('+')
		SERNODEIP = str(WORDS2[2])
		NP = int(WORDS2[3])
		SERNODEPORT = int(NP)
		SERNODEADDR = (str(SERNODEIP), int(SERNODEPORT))# SELECTING THE ADDRESS FROM WHICH THE SEARCH REQUEST CAME
		PARENT_REQ = SERNODEADDR
		DOCNAME = MSSGG[1]				# SELECTING THE FILENAME TO BE SEARCHED
		HOPS = str(MSSGG[2])				# SELECTING THE HOP COUNT
		size = len(FILES)
		flag = 0
		if (HOPS < "5"):
			counter = 0
			while (counter < size):
				a = FILES[counter]
				if (str(a) == str(DOCNAME)):
					flag = 1
				else:
					FILES.append(str(DOCNAME))
				counter = counter + 1
			if (flag == 1):
				print str(DOCNAME)+" "+"ALREADY EXECUTED IN: "+str(ADDR_NODE)# PRINTING IF THE FILENAME HAS BEEN CHECKED
				
			else:
				F15 = open("QUERY.txt", "a")
				F15.write(DOCNAME)
				F15.close()
				SERVAL = SEARCHOWN(DOCNAME);		# SEARCHING FOR THE FILENAME IN ITS OWN DATABASE OF MYFILE.TXT
				if (SERVAL != "0"):
					HOPCOUNT = int(HOPS)
					HOPCOUNT = HOPCOUNT + 1		# INCREASING HOP COUNT BY 1
					HOPS = str(HOPCOUNT)
					QNTY = str(SERVAL)
					serretmsg = "SEROK"+" "+str(QNTY)+" "+str(MY_HOSTADDR)+" "+str(DOCNAME)+" "+str(HOPS)
					r = int(len(serretmsg))
					s = r + 4
					SERRETMSG ="00"+str(s)+" "+str(serretmsg)
					print str(SERRETMSG)
					sok.sendto(SERRETMSG, SERNODEADDR)# SENDING THE SEARCH REPLY TO THE CONTROLLER
				else:
					HOPCOUNT = int(HOPS)
					HOPCOUNT = HOPCOUNT + 1
					HOPS = str(HOPCOUNT)
					QNTY = str(SERVAL)
					serretmsg = "SEROK"+" "+str(QNTY)+" "+str(MY_HOSTADDR)+" "+str(DOCNAME)+" "+str(HOPS)
					r = int(len(serretmsg))
					s = r + 4
					SERRETMSG="00"+str(s)+" "+str(serretmsg)				
					print str(SERRETMSG)
					SERVAL1 = SEARCH(DOCNAME, HOPS);# SENDING SEARCH COMMAND TO THE NODES PRESENT IN THE ROUTING TABLE
				
				
		else:
			print "HOP COUNT: " +str(HOPS)
			print "HOPS COUNT EXCEEDED (MAX: #5)..!! SEARCH NOT POSSIBLE..!!"# DISPLAY NO SEARCH IF HOP COUNT EXCEEDS
			#LEVMSG = LEAVE(MY_HOSTADDR, MY_PORT, MY_USR);
		
	elif (WORDS2[1] == "LEAVE"):					# LEAVE COMMAND RECEIVED 
		print "LEAVE MESSAGE RECEIVED"+str(ADDR_NODE)		# LEAVEOK DEFINATION
		leav_msg = "LEAVEOK"+" "+"0"
		r = int(len(leav_msg))
		s = r + 4
		LEAVOKMSG = "000"+str(s)+" "+str(leav_msg)
		sok.sendto(LEAVOKMSG, ADDR_NODE)			# SENDING LEAVEOK MESSAGE
		LEVMSG = LEAVE(MY_HOSTADDR, MY_PORT, MY_USR);		# CALLING LEAVE MESSAGE
		
	elif (WORDS2[1] == "JOINOK"):					# JOIN OK COMMAND RECEIVED
		print "JOINOK RECEIVED FROM: "+str(ADDR_NODE)		
		print str(MSSG_NODE)					# PRINTING JOINOK
		
	else:
		print str(MSSG_NODE)					# UNKNOWN COMMAND
	
	



