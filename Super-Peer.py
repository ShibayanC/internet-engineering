#-------------------------------------------------------#
#-----------Controller Program, for PA01----------------#
#---------Shibayan Chatterjee, CONPA01, CS 581----------#
#-------------------------------------------------------#

# importing modules
import socket									# Importing Socket Module
import os
import md5
from random import *
import sys
import select
import math
os.system("clear")

nodeAddress = []								# List for storing the node Addresses present in the network
WCarray = []									# List of files for WC Database
LBLarray = []									# List of files for LBL Database
SDSCarray = []									# List of files for SDSC Database

def client(msgIP, port, data):					# Defining Client Module
	IP = str(msgIP)							
	PORT =int(port)							
	DATA = data								
	try:
		client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)			# Defining Client Socket
		print "Client socket created..!!"
	except	socket.error, (value, message):
		print "Could not create client socket: " + message
	try:
		client.connect((IP, PORT))											# Connecting Address for Client
	except	socket.error, (value, message):
		print "Connection not successfull..!!"+str(IP)+" "+str(PORT)
		return
	
	client.send(DATA)														# Sending Message from Client Socket
	recv_msg = client.recv(3000)											# Received Message at Client Socket
	print "Message received in Client Socket: " +str(recv_msg)
	client.close()															# Closing the Client Socket
	return recv_msg															# Received Message at Client Socket

def putOperation(f, putIP, putPort, myIPAddress, myPort):					# Defining PUT operation
	l = int(len(f))
	myIPAddress = str(myIPAddress)
	myPort = int(myPort)
	for i in range(1, l):
		fname = str(f[i])
		msg = "PUT"+" "+str(fname)											# PUT format: PUT <filename>
		putIP = str(putIP)
		putPort = int(putPort)
		reply = client(putIP, putPort, msg)									# Sending message to the peer through Client Socket
		w = reply.split()													# PUTOK format: PUTOK <filename> 
		if (str(w[1]) == str(fname)):
			print "File successfully added in network..!!"
		else:
			print "File "+str(fname)+ " not added..!!"
	return

def getOperation(choice, nodeAddress, WCarray, LBLarray, SDSCarray):		# Defining GET operation
	if int (choice) == 1:
		f = WCarray
		print "'World Cup' dataset to be used in GET operation.."
	elif int(choice) == 2:
		f = LBLarray
		print "'lbl-conn-7' dataset to be used in GET operation.."
	elif int(choice) == 3:
		f = SDSCarray
		print "'sdsc-http' dataset to be used in GET operation.."
	else:
		print "Unknown Choice"
	l = int(len(f))
	k = 0
	x = int(randint(1, l))
	x = x - 1
	ranIP = str(nodeAddress[x][0])
	ranPort = int(nodeAddress[x][1])
	print "Randomly Selecting IP: "+str(ranIP)+" Port: "+str(ranPort)+ " for GET command"
	for k in range(l):
		fname = str(f[i])
		msg = "GET"+" "+str(fname)											# GET format: GET <filename>
		reply = client(ranIP, ranPort, msg)									# Calling Client Socket for GET Operation 							
		w = reply.split()
		if (str(w[1]) == str(fname)):										# GETOK format: GETOK <filename> <IP Address>
			print "File "+str(fname)+" present at: "+str(w[2])
		else:
			print "File not found in the network..!!"
	return

def main():																	# Defining Main Function
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)					# Defining Server Socket
	myHostAddress = str(socket.gethostname())
	myIPAddress = str(socket.gethostbyname(myHostAddress))
	myPort = int(sys.argv[1])												# User input for Port no.
	myAddress = (str(myIPAddress), int(myPort) )
	print "My Address is: " + str(myAddress)
	print "I am the external node for GET and PUT command"
	try:
		s.bind((myIPAddress, myPort))										# Binding socket to the port
	except socket.error, msg:
		print "Socket Bind failed. Error Code: " + str(msg[0]) + ' Message ' + str(msg[1])
		s.close()
		exit(0)
	print "Socket Binded successfully: " + str(myAddress)
	
	n = int(raw_input("Enter the no of nodes present in the network: "))	# User input for number nodes present in network
	i = 0
	for i in range(n):
		ip = str(raw_input("Enter the IP Address for: "	+str(i + 1)+ " node: "))
		port = int(raw_input("Enter the port nos: " +str(i + 1) + " node: "))
		address = [ip, port]												# Appending IP Address and Port nos of Peers
		nodeAddress.append(address)
	l = int(len(nodeAddress))
	print "Total no. of address: " +str(l)
	print "List of Addresses: "
	i = 0
	for i in nodeAddress:
		print (i)
	x = int(randint(1, l))
	x = x - 1
	ranIP = str(nodeAddress[x][0])
	ranPort = int(nodeAddress[x][1])
	print "Randomly Selecting IP: "+str(ranIP)+" Port: "+str(ranPort)+ " for PUT command"		# Randomly selecting IP Address and Port for PUT
	print "I have the below list of dataset: "
	print "1. WorldCup"
	print "2. lbl-conn-7"
	print "3. sdsc-http"
	choice = int(raw_input("Enter your choice to insert keys(only one out of 1,2,3): "))	# User input for the Database to be inserted
	f = open( "WC.txt", "r" )
	for line in f:
		WCarray.append(line)											# Putting files in a list
	f.close()
	
	f = open("lbl-conn-7.txt","r")
	for line in f:
		LBLarray.append(f)												# Putting files in a list
	f.close()
	
	f = open("sdsc-http.txt","r")
	for line in f:
		SDSCarray.append(f)												# Putting files in a list
	f.close()
	if int(choice) == 1:
		print "'World Cup' dataset to be used in PUT operation.."
		putOperation(WCarray, ranIP, ranPort, myIPAddress, myPort)		# PUT operation for filenames
	elif int(choice) == 2:
		print "'lbl-conn-7' dataset to be used in PUT operation.."
		putOperation(LBLarray, ranIP, ranPort, myIPAddress, myPort)		# PUT operation for filenames
	elif int(choice) == 3:
		print "'sdsc-http' dataset to be used in PUT operation.."
		putOperation(SDSCarray, ranIP, ranPort, myIPAddress, myPort)	# PUT operation for filenames
	else:
		print "Invalid Choice..!!"
		
	print "Performing GET Keys.."										# Initiation of GET operation
	getOperation(choice, nodeAddress, WCarray, LBLarray, SDSCarray)		# GET function called
	s.close()															# Closing of Server Socket
	exit(0)
	
if __name__ == "__main__":
	main()																# Calling Main function
