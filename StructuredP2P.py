#-----------------------------------------------#
#-------------IMPORTING MODULES----------------#
import socket
import os
import select
import time
import md5
import sys
import random
import threading
import math
import random

os.system("clear")

BUFF = 1026
serCount = 0
#------------------------------------------------#
#------------ DEFINING LISTS---------------------#
forwardFile = []
MYFILE = []
MY_FILE = []
HASH_MYFILE = []
FINGER_TABLE = []
IP = []
PORT = []
KEY = []
interval = []
FILE = []
ADDFILE = []
SEARCH = []

#----------------------------------------------------#
#----------SEARCH COMMAND DEFINATION-----------------#

def search(filename, IP, PORT, hops, msg):	# DEFINING SEARCH COMMAND
	m = md5.new()
	m.update(str(filename))
	filehash = m.hexdigest()
	fileHash = filehash[-8:]				# HASHING THE FILENAME
	FILE_HASH = int(fileHash, 16)
	KEY = int(FILE_HASH % 32)			# GETTING THE VALUE FOR THE FILENAME
	print "KEY FOR THE SEARCH FILE: "+str(KEY)
	ipQuery = str(IP)
	portQuery = int(PORT)
	value = int(len(FINGER_TABLE))
	val = int(value)
	x = 0
	hopCount = int(hops)
	flag = 0
	hopCount = hopCount + 1				# INCREASE THE HOP COUNT
	key = int(KEY)
	while (int(x) < int(val)):
		a = int(FINGER_TABLE[x][1])		# SELECTING THE ENTRY
		if (int(a) >= int(key)):			# SELECT THE HIGHEST ENTRY LESS THAN THE FINGER TABLE
			forwardip = str(FINGER_TABLE[x][2])		# SELECTING THE FORWARDING IP
			forwardport = int(FINGER_TABLE[x][3])		# SELECTING THE FORWARDING PORT
			SEARCH.append([forwardip, forwardport])		# MAKING A LIST FOR THE SEARCH IP, SEARCH PORT 
			flag = 1
			#x = x + 1
			
		elif ((int(a) < int(key)) and (int(a) == 0)):	# SELECTING IF THE ENTRY IS ZERO
			forwardip = str(FINGER_TABLE[x][2])		# SELECTING THE FORWARDING IP
			forwardport = int(FINGER_TABLE[x][3])		# SELECTING THE FORWARDING PORT
			flag = 2
		else:
			print " "
			#x = x + 1
			#flag = 0
		x = x + 1
	for i in SEARCH:
		print i	
	if (int(flag) == 1):
		forwardip = str(SEARCH[0][0])				# READING THE IP FROM THE SEARCH LIST
		forwardport = int(SEARCH[0][1])			# READING THE PORT NOS FROM THE SEARCH LIST
		print "FORWARDING SEARCH MESSAGE TO: "+str(forwardip)+" "+str(forwardport)
		newsermsg = "SER"+" "+str(ipQuery)+" "+str(portQuery)+" "+str(filename)+" "+str(hopCount)	# DEFINING SEARCH COMMAND
		r = int(len(newsermsg))
		s = r + 4
		newmsg1 = "00"+str(s)+" "+str(newsermsg)
		print str(newmsg1)						# PRINTING THE SEARCH COMMAND
		newmsg = Client(forwardip, forwardport, newmsg1)	# FORWARDING THE SEARCH COMMAND
		print "SEARCH REPLY: "+str(newmsg)		
	
	else:
		if(int(flag) == 2):
			print "FORWARDING SEARCH MESSAGE TO: "+str(forwardip)+" "+str(forwardport)	
			newsermsg = "SER"+" "+str(ipQuery)+" "+str(portQuery)+" "+str(filename)+" "+str(hopCount)
			r = int(len(newsermsg))
			s = r + 4
			newmsg1 = "00"+str(s)+" "+str(newsermsg)
			print str(newmsg1)
			newmsg = Client(forwardip, forwardport, newmsg1)	# FORWARDING THE SEARCH COMMAND
			print "SEARCH REPLY: "+str(newmsg)				# READING THE SEARCH REPLY
		
		else:
			newsermsg = "SER"+" "+"0"+" "+str(MY_HOSTIP)+" "+str(MY_PORT)+" "+str(filename)+" "+str(hopCount)
			r = int(len(newsermsg))
			s = r + 4
			newmsg = "00"+str(s)+" "+str(newsermsg)
			#print str(newmsg)
			#ser = Client(forwardip, forwardport, newmsg)
			print "SEARCH REPLY: "+str(newmsg)				# IF UNSUCCESSFUL SEARCH DONE
			print "SEARCH NOT SUCCESSFUL..!!"
	return (newmsg)									# RETURNING THE SEARCH MESSAGE
				
#----------------------------------------------------#
#---------------CREATING MYFILE.txt------------------#
#----------------------------------------------------#
def myFile(value):
	f2 = open("filename.txt", "r+")						# OPENING filename.txt
	files = f2.readlines()								
	x = int(len(files))
	nos = int(raw_input("ENTER THE NOS OF FILES TO BE SELECTED (max: #10): "))	# USER INPUT FOR NOS OF FILS TO BE SELECTED
	count = 0
	while (count < nos):
		y = random.randrange(1,x)				# RANDOMLY SLECTING FILES
		filename = files[y]
		fil_name = filename[:-1]
		MYFILE.append(str(fil_name))				# ADDING THE FILES IN MY_FILE.txt
		count = count + 1
	print "---------------------------------------"
	print "--------CONTENTS OF MYFILE.txt-------- "
	print "---------------------------------------"
	i = 0
	for i in MYFILE:
		print i
	val2 = hashing_myfile(MYFILE, value);			# CALLING THE HASH FUNCTION FOR HASHING THE FILENAMES
	
#-------------------------------------------------------------#
#------------------HASHING MY FILE.txt------------------------#
#-------------------------------------------------------------#
def hashing_myfile(filenames, value):
	values = int(2**value)					
	length = int(len(filenames))
	count = 0
	while (count < length):
		#print "HASHING FOR FILE: "+str(filenames[count])
		fil = filenames[count]
		m1 = md5.new()
		file_lower = fil.lower()					# CONVERTING THE FILENAMES TO LOWER CASE
		new_file = file_lower.replace(" ", "_")		# REPLACING THE SPACE IN FILENAMES WITH "_"
		m1.update(str(new_file))
		h1 = m1.hexdigest()
		H1 = int(h1[-8:], 16)
		h_val = H1 % values						# GETTING THE VALUE FOR THE FILE
		#print str(H1)
		MY_FILE.append([(str(h_val)), (str(new_file))])		# APPENDING THE FILES
		words2 = file_lower.split()
		#print str(words2)
		l = int(len(words2))
		count1 = 0
		
		if (l == 1):
			count = count + 1
		else:
			while (count1 < l):
				m2 = md5.new()
				wor = words2[count1]
				wor_lower = wor.lower()
				m2.update(str(wor_lower))
				h2 = m2.hexdigest()
				H2 = int(h2[-8:], 16)
				h2_val = H2 % value
				#print str(H2)
				MY_FILE.append([str(h2_val), str(new_file)])		# APPENDING EVERY WORD KEY WITH THE CORRESPONDING FILENAME
				count1 = count1 + 1
			count = count + 1
	print "--------------------------------------------------------------------"
	print "HASH KEYS FOR MY FILES: "
	print "--------------------------------------------------------------------"
	i = 0
	for i in MY_FILE:
		print i
	print "---------------------------------------------------------------------"


#-------------------------------------------------------------------------#
#-------------------------------CLIENT MODE-------------------------------#
#---------------CLIENT SOCKET CREATION, send(), recv() only---------------#

def Client(msgIP, port, data):					# DEFINING CLIENT SOCKET FUNCTION
	IP = str(msgIP)							# IP ADDRESS TO WHICH THE CLIENT WILL BE SENDING MESSAGE
	PORT =int(port)							# PORT TO WHICH THE CLIENT WILL BE SENDING MESSAGE
	DATA = data								# DATA TO BE SEND
	try:
		client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)	# INITIALIZING CLIENT SOCKET
		#print "Client socket created..!!"
	except	socket.error, (value, message):
		print "Could not open client socket: " + message
	
	try:
		client.connect((IP, PORT))				# CONNECTING TO NECESSARY IP AND PORT
	except	socket.error, (value, message):
		print "Connection not successfull..!!"+str(IP)+" "+str(PORT)
		return
			
	client.send(DATA)							# SENDING DATA TO NODE
	recv_msg = client.recv(3000)					# RECEIVING REPLY FROM THE NODE
	#print str(recv_msg)
	client.close()								# CLOSING THE CLIENT SOCKET
	return recv_msg							# RETURNING THE CLIENT REPLY

#--------------------------------------------------------------#
#-------------DEFINING UNREG COMMAND RECEIVED------------------#
#--------------------------------------------------------------#	
def UNREG(unreg_text, IP, PORT):					# DEFINING UNREGISTER FUNCTION
	print "UNREGISTERING NODE..!!"
	print str(unreg_text)
	DATA = unreg_text							# UNREGISTER MESSAGE
	BS_IP = IP								# SELECTING BS IP
	BS_PORT = PORT								# SELECTING BS PORT
	unreg_reply = Client(BS_IP, BS_PORT, DATA)		# CALLING THE CLIENT FUCTION
	print "MESSAGE AFTER UNREGISTERING..!!"
	print str(unreg_reply)						# REPLY FROM BS
	return 0

#---------------------------------------------------------------#
#---------------SPLITTING THE BOOSTRAP MESSAGE------------------#
#---------------------------------------------------------------#
	
def split_msg(text):							# SPLITTING THE BS MESSAGE
	w = text.split()
	x = int(len(w))
	if (x <= 3):
		return w[2]
	else:
		l = 3
		while (l < x):
			#print l
			#print str(w[l])
			IP.append(w[l])					# STORING THE IPs OF ALL NODES WHICH HAVE ALREADY REGISTERED
			l = l + 1
			PORT.append(w[l])					# STORING THE PORT OF ALL NODES WHICH HAVE ALREADY REGISTED 
			l = l + 1
			KEY.append(w[l])					# HASH KEY OF ALL THE NODES WHICH HAVE ALREADY REGISTERED
			l = l + 1
		
	#print "IP: PORT: KEY: "
	#print str(IP)
	#print str(PORT)
	#print str(KEY)
	
#-------------------------------------------------------------#
#------------------ANALYSIS OF NODE MESSAGE-------------------#
#-------------------------------------------------------------#

def split_nodemsg(message):
	words3 = message.split()
	return words3[2]


#------------------------------------------------------------#
#------------------INITIALIZING FINGER TABLE-----------------#
#------------------------------------------------------------#

def FINGERTABLE(IP, PORT, KEY, bits):				# DEFINING FINGER TABLE FUNCTION
	M = int(bits)
	value = int(2**(M))
	i = 1
	x = 1
	func = 0
	ip = str(IP)
	port = int(PORT)
	n = int(KEY)
	myValue = int(KEY%value)
	print "MY VALUE IS: "+str(myValue)
	succ = int(myValue)
	interval = [succ, succ]
	while (x <= M):
		f = (pow(2,(x-1))+n)					# SELECTING THE ENTRY
		func = int(f%value)						# VALUE FOR THE ENTRY
		FINGER_TABLE.append([func, myValue, ip, port])	# ADDING LINES TO THE FINGER TABLE
		x = x + 1
		succ = func
	i = 0
	print "------------------------------"
	print "---------FINGER TABLE---------"
	print "------------------------------"
	for i in FINGER_TABLE:
		print i								# PRITING THE FINGER TABLE
	
	return 
#--------------------------------------------------------------#
#-------------DEFINING UPFINGER TABLE COMMAND------------------#
#--------------------------------------------------------------#			
	
def UPFIN(nos):							# DEFINING UPDATE FINGER TABLE
	#n = int(nos)
	print "\n"
	print "-----------------------------------------------------------------------"
	print "SENDING COMMAND TO"+" "+str(nos)+" "+"NODE TO UPDATE FINGER TABLE..!!"
	i = 0
	n = int(len(IP))
	N = int(n)
	#print str(N)
	while (int(i) < int(N)):
		IP_addr = IP[i]					# SELECTING IP ADDRESS
		PORT_addr = PORT[i]					# SELECTING PORT ADDRESS
		KEY_addr = KEY[i]					# SELECTING THE KEY
		VAL = 0
		upfin_msg = "UPFIN"+" "+str(VAL)+" "+str(MY_HOSTIP)+" "+str(MY_PORT)+" "+str(myHash)
		r = int(len(upfin_msg))
		s = r + 4
		UPFIN_MSG = "00"+str(s)+" "+str(upfin_msg)		# DEFINING THE UPDATE FINGER MESSAGE
		i = i + 1
		#print str(UPFIN_MSG)
		upfinrecv_msg = Client(IP_addr, PORT_addr, UPFIN_MSG)		# start the client mode and send the message
		#print str(upfinrecv_msg)
		words4 = upfinrecv_msg.split()
		if (str(words4[1]) == "UPFINOK"):
			hashkey = words4[2]
			print "UPDATE OK FINGER_TABLE RECEIVED FROM: !!"+str(IP_addr)+" "+str(PORT_addr)
			print "--------------------------------------------------------------------------"
			print str(upfinrecv_msg)
			cond = 0
			print "\n"
			print "--------------------------------------------------------------------------"
			val = updateFingertable(IP_addr, PORT_addr, hashkey, bits, myHash, cond);		# UPDATING OWN FINGER TABLE
			print "MY FINGER TABLE IS UPDATED..!!"
			print "--------------------------------------------------------------------------"
			
	k = int(len(IP))
	K = int(k)
	l = 0
	return 0
		
#------------------------------------------------------------------------#
#---------------------DEFINING UNFINOK MESSAGE---------------------------#
#------------------------------------------------------------------------#
def updateFingertable(ipAddr, port, key, bit, myhash, val):		# UPADTE FINGER TABLE FUNCTION
	if (int(val) == 0):
		print "UPDATING FINGER TABLE FOR: IP:"+str(ipAddr)+" PORT: "+str(port)+" HASH VALUE: "+str(key) 
		#print str(key)
		k = int(key, 16)
		#print str(bit)
		bitsp = int(2**bit)
		#print str(bitsp)
		v = int(k % bitsp)
		val = int(v)								# GETTING KEY FOR THE INCOMING NODE
		#print "VALUE FOR THE INCOMING NODE: "+str(val)
		
		k1 = int(myhash, 16)
		k2 = int(k1 % bitsp)
		myval = int(k2)							# GETTING KEY FOR THE CURRENT NODE
		#print "VALUE OF MY NODE: "+str(myval)
		
		print "NODE IP: "+str(ipAddr)+" PORT: "+str(port)+" KEY: "+str(val)+" JOINING THE NETWORK..!!"
		if (int(myval) == int(val)):
			#print "MATCHING KEY FOUND..!! NO UPDATION..!!"
			return
		else:
			i = int(len(FINGER_TABLE))
			j = int(i)
			count = 0
			#-------------LOGIC FOR FINGERTABLE UPDATION------------------#
			while (int(count) < int(j)):
				a = int(FINGER_TABLE[count][0])
				b = int(FINGER_TABLE[count][1])
				c = int(val)
				if (int(FINGER_TABLE[count][1]) == int(myval)):
					FINGER_TABLE[count][1] = val
					FINGER_TABLE[count][2] = str(ipAddr)
					FINGER_TABLE[count][3] = int(port)
					count = count + 1
				else:
					if ((int(a) > int(myval)) and (int(a) < 32)):
						d1 = int(a - myval)
						#print "A"
						#print "d1: "+str(d1)
					elif ((int(a) < int(myval)) and (int(a) < 32)):
						d1 = int((32 - myval) + a)
						#print "B"
						#print "d1: "+str(d1)
					else:
						print "B1"
						
					if ((int(b) > int(myval)) and (int(b) < 32)):
						d2 = int(b - myval)
						#print "C"
						#print "d2: "+str(d2)
					elif ((int(b) < int(myval)) and (int(b) < 32)):
						d2 = int((32 - myval) + b)
						#print "C1"
						#print "d2: "+str(d2)
					else:
						print "C2"
						
					if ((int(c) > int(myval)) and (int(c) < 32)):
						d3 = int(c - myval)
						#print "D1"
						#print "d3: "+str(d3)
					elif ((int(c) < int(myval)) and (int(c) < 32)):
						d3 = int((32 - myval) + c)
						#print "D2"
						#print "d3: "+str(d3)
					else:
						print "D3"
					
					if (int(d2) < int(d1)):
						FINGER_TABLE[count][1] = int(val)
						FINGER_TABLE[count][2] = str(ipAddr)
						FINGER_TABLE[count][3] = int(port)
						#print "G"
						
					elif ((int(d3) >= int(d1)) and (int(d3) < int(d2))):
						FINGER_TABLE[count][1] = int(val)
						FINGER_TABLE[count][2] = str(ipAddr)
						FINGER_TABLE[count][3] = int(port)
						#print "E"	
					else:
						print "F"
						
					count = count + 1
	
	elif (int(val) == 1):				# DEFINATION WHEN THE NODE IS LEAVING THE NETWORK
		print "-----------------------------------------------------------------"
		print "UPDATING FINGER TABLE FOR: IP: "+str(ipAddr)+" PORT: "+str(port)
		print str(key)
		k = int(key, 16)
		bitsp = int(bit)
		v = int(k%bitsp)
		val = int(v)
		k1 = int(myhash%bit_sp)
		myval = int(k1)
		print "NODE "+str(ipAddr)+" "+str(port)+" KEY: "+str(val)+" LEAVING THE NETWORK..!!"
		i = int(len(FINGER_TABLE))
		j = int(i)
		count = 0
		while (int(count) < int(j)):			# SELECTING THE IP ADDRESS WHICH IS LEAVING THE NETWORK
			a = FINGER_TABLE[count][0]
			if (int(val) == int(a)):
				replace = int(count)
				count = count + 1
			else:
				count = count + 1
				
		while (int(count) < int(j)):
			if (int(replace) == int(count)):		# SLECTING THE SUCCESSOR IP ADDRESS
				count = count + 1
				FINGER_TABLE[replace][1] = FINGER_TABLE[count][1]		# REPLACING KEY
				FINGER_TABLE[replace][2] = FINGER_TABLE[count][2]		# REPLACING IP
				FINGER_TABLE[replace][3] = FINGER_TABLE[count][3]		# REPLACING PORT
				count = count + 1
			else:
				count = count + 1
	else:
		print "UNKNOWN COMMAND FOR UPDATING FINGER TABLE..!!"

	flag = 0
	print "-------------------------------------------------"
	print "-------------------FINGER TABLE------------------"
	print "-------------------------------------------------"
	for i in FINGER_TABLE:						# PRINTS UPDATED FINGER TABLE
		print i
	print "-------------------------------------------------"
	return (flag)

#----------------------------------------------------#
#---------DEFINING GETKEY DEFINATION-----------------#
#----------------------------------------------------#

def getKey(myHash, bits):				# DEFINING THE GETKY DEFINATION
	print "\n"
	print "---------------------------------------------------"
	print "GET KEY COMMAND INITIATED..!!"
	#print str(myHash)
	getkeymsg = "GETKY"+" "+str(myHash)
	l = 3
	r = int(len(getkeymsg))
	s = r + 4
	getKeymsg = "00"+str(s)+" "+str(getkeymsg)
	x = int(len(IP))
	y = int(x)
	count = 0
	while (int(count) < int(y)):
		ipForward = IP[count]
		portForward = PORT[count]
		getkyval = Client(ipForward, portForward, getKeymsg);		# SENDING THE GETKY COMMAND
		print "MESSAGE RECEIVED FOR GETKY FROM: "+str(ipForward)+" "+str(portForward)
		print "MESSAGE: "+str(getkyval)
		count = count + 1
		words = getkyval.split()
		if (str(words[2]) == "9997"):
			print "NO FILES TO ADD..!!"
			print "-------------------------------------------------------"
			flag = 0
		else:
			a = int(len(words))
			A = int(a)
			l = 2
			while (int(l) < int(A)):
				addip = words[l]
				l = l + 1
				#print str(words[l])
				addport = words[l]
				#print str(addport)
				l = l + 1
				#print str(words[l])
				addkey = words[l]
				#print str(words[l])
				l = l + 1
				#print str(words[l])
				addfile = words[l] 
				#print str(words[l])
				ADDFILE.append([addip, addport, addkey, addfile])		# CREATING A TEMPORARY FILE FOR SELECTING THE IP, PORT, FIELKEY, FILENAME
				l = l + 1
	
		#print "Contents of ADDFILE: "
		#for i in ADDFILE:
		#	print i
		flag = 1
	
	return (flag)
	
#-----------------------------------------------------------#
#-----------------DEFINING GETKYOK FUNCTION-----------------#
#-----------------------------------------------------------#

def getkeyok(recvkey, myHash, bits, ipkey, portkey):		# GETKY DEFINATION
	a = int(2**bits)
	b = int(recvkey, 16)
	c = int(b % a)    #----Key of the incoming node
	x = int(myHash, 16)
	flag = 0
	y = int(x % a)
	if (int(y) < int(c)):
		CCW = int((y - 0) + (32 - c))		# SELECTING THE ANTICLOCKWISE RANGE FOR GETKY
		#print "LENGTH OF FILES: "+str(CCW)
		flag = 1
	else:
		CCW = int(y - c)
		#print "LENGTH OF FILES: "+str(CCW)
		flag = 2
	l = int(len(MY_FILE))
	size = int(l)
	count = 0
	
	if (str(flag) == "1"):
		while (int(count) < int(size)):
			myfilekey = int(MY_FILE[count][0])
			if ((myfilekey in range (32, c)) and (myfilekey in range(0, y))):
				k = myfilekey
				l = str(MY_FILE[count][1])
				FILE.append([k, l])
				count = count + 1
			else:
				count = count + 1
	elif (str(flag) == "2"):
		while (int(count) < int(size)):
			myfilekey = int(MY_FILE[count][0])
			if (myfilekey in range (c, y)):
				k = myfilekey
				l = str(MY_FILE[count][1])
				FILE.append([k, l])
				count = count + 1
			else:
				count = count + 1
	else:
		print "ERROR OCCURRED WHILE SELECTING FILES..!!"
		return
	
	print "CONTENTS OF FILE TO BE TRANSFERRED..!!"
	#print str(FILE)
	#for i in FILE:
	#	print i
	m = int(len(FILE))
	newm = int(m)
	n = int(len(FINGER_TABLE))
	newn = int(n)
	l1 = " "
	l2 = " "
	l3 = " "
	#line = []
	total = " "
	count = 0
	count1 = 0
	while (int(count) < int(newm)):
		file1 = FILE[count][1]
		filekey = FILE[count][0]
		while (int(count1) < int(newn)):
			if (str(filekey) <= str(FINGER_TABLE[count1][0])):
				#print str(filekey)
				#print str(FINGER_TABLE[count1][0])
				ip = str(FINGER_TABLE[count1][2])
				l = str(ip)
				#print str(l)
				port = int(FINGER_TABLE[count1][3])
				l1 = str(l)+" "+str(port)
				#print str(l1)
				key = str(filekey)
				l2 = str(l1)+" "+str(key)
				#print str(l2)
				l3 = str(l2)+" "+str(file1)
				#print str(l3)
				#print str(count1)
				count1 = count1 + 1
				total = total+" "+str(l3)		# MAKING THE GETKY STATEMENT
			else:
				count1 = count1 + 1
				#str(l3) = " "
		count1 = 0
		count = count + 1
		
	#print str(total)
	return (total)	

#-------------------------------------------------------------#
#---------------ADD FUNCTION DEFINATION----------------------#
#-------------------------------------------------------------#

def addkeydef():
	x = int(len(ADDFILE))
	y = int(x)
	count = 0
	#words = msg.split()
	while (int(count) < int(y)):
		if (str(ADDFILE[count][0]) == str(MY_HOSTIP)):
			addfilekey = str(ADDFILE[count][2])
			addfilename = str(ADDFILE[count][3])
			print "\n"
			print "--------------------------------------------------------------"
			print "ADDING KEY: "+str(addfilekey)+" FILE: "+str(addfilename)
			list1 = [addfilekey, addfilename]
			MY_FILE.append([addfilekey, addfilename])
			print "KEY: "+str(addfilekey)+" FILE: "+str(addfilename)+" SUCCESSFULLY ADDED..!!"
			print "--------------------------------------------------------------"
			count = count + 1
	
		else:
			print "ADD COMMAND NOT SUCCESSFULL..!!"
			count = count + 1
	print "---------------------------------------------"	
	print "NEW CONTENTS OF MY FILE: "
	for i in MY_FILE:
		print i
	print "---------------------------------------------"
	val6 = addfunc()
	return 0
#--------------------------------------------------------------#
#----------------------ADDKEY DEFINATION-----------------------#
#--------------------------------------------------------------#

def addkeynode (msg):				# ADD KEY DEFINATION
	print "EXECUTING MESSAGE RECEIVED: "+str(msg)
	words = msg.split()
	ipAddr = words[2]
	portAddr = words[3]
	filekey = words[4]
	filename = words[5]
	if (str(ipAddr) == str(MY_HOSTIP)):			# CHECK CONDITION WHETHER THE DESTINATION IP IS THE SAME
		x = int(len(MY_FILE))
		y = int(x)
		count = 0
		flag = 0
		while (int(count) < int(y)):
			if (int(filekey) == str(MY_FILE[count][0])):	# IF THE FILE IS PRESENT ALREADY IT DOES'NT ADD
				print str(filename)+" "+"FILE ALREADY PRESENT.."
				count = count + 1
				flag = 1
			else:
				count = count + 1
				flag = 0
		if (str(flag) == "0"):
			MY_FILE.append([filekey, filename])		# APPENDS THE FILENAME IF NOT PRESENT
		print "\n"
		print "-------------------------------------------------------"
		for i in MY_FILE:
			print i
		print "-------------------------------------------------------"
		return 0
	else:
		print "WRONG DESTINATION REACHED FOR ADD COMMAND..!!"
		return 1
	
#--------------------------------------------------------------#
#-------------------ADD COMMAND DEFINATION---------------------#
#--------------------------------------------------------------#

def addfunc():								# DEFINING ADD FUNCTION
	print "SENDING ADD COMMAND TO NODES..!!"	
	len1 = int(len(ADDFILE))
	flag = 0
	len2 = int(len1)
	count = 0
	while (int(count) < int(len2)):
		addip = ADDFILE[count][0]			# SELECTING THE IP ADDRESS TO WHICH ADD COMAND IS TO BE SEND
		addport = ADDFILE[count][1]			# SELECTING THE PORT TO WHICH THE ADD COMMAND IS TO BE SEND
		addkey = ADDFILE[count][2]			# SELECTING KEY
		addfile = ADDFILE[count][3]			# SELECTING ADDFILE
		if (str(addip) != str(MY_HOSTIP)):
			addmsg = "ADD"+" "+str(addip)+" "+str(addport)+" "+str(addkey)+" "+str(addfile)
			r = int(len(addmsg))
			s = r + 4
			addMsg = "00"+str(s)+" "+str(addmsg)
			print str(addMsg)
			addfile = Client(addip, addport, addMsg)	# CALLING TEH CLIENT SOCKET FOR SENDING THE ADD MSG 
			print "MESSAGE RECEIVED AFTER ADD COMMAND FROM: "+str(addip)+" "+str(addport)
			print "MESSAGE: "+str(addfile)
			count = count + 1
			flag = 1
		else:
			count = count + 1
	if (int(flag) != 1):
		print "NO FILE TO GENERATE ADD COMMAND..!!"
		print "--------------------------------------------------------------"
	else:
		print "ADD COMMAND EXECUTED SUCCESSFULLY..!!"
		print "--------------------------------------------------------------"

#--------------------------------------------------------------#
#-----------------LEAVE COMMAND DEFINATION---------------------#
#--------------------------------------------------------------#
def leavenode(myHash):					# DEFINING LEAVE NODE
	x = int(len(FINGER_TABLE))				
	count = 0
	flag = 0
	cond = 1
	leavecomm = "UPFIN"+" "+str(cond)+" "+str(MY_HOSTIP)+" "+str(MY_PORT)+" "+str(myHash)
	r = int(len(leavecomm))
	s = r + 4
	leavemsg = "00"+str(s)+" "+str(leavecomm)		# DFINING LEAVE MSG
	while (int(count) > int(x)):
		ipforward = FINGER_TABLE[count][2]
		portforward = FINGER_TABLE[count][3]
		leavret = Client(ipforward, portforward, leavemsg)		# SENDING LEAVE COMMAND
		words = leavret.split()
		if (str(words[1]) == "UPFINOK"):
			print "UPFINOK RECEIVED FROM: "+str(ipforward)+" "+str(portforward)
			print "MESSAGE: "+str(leavret)
			flag = 1
		else:
			print "UNKNOWN MESSAGE RECEIVED: "+str(ipforward)+" "+str(portforward)
			print "MESSAGE: "+str(leavret)
		count = count + 1
	
	return (str(flag))
#--------------------------------------------------------------#
#-------------------GIVEKEY DEFINATION-------------------------#
#--------------------------------------------------------------#
def givekey():
	x = int(len(MY_FILE))
	y = int(len(FINGER_TABLE))
	count = 0
	l = " "
	l1 = " "
	total = " "
	count1 = 0
	while (int(count) < int(x)):
		nodeentry = int(FINGER_TABLE[count][1])
		nodeip = int(FINGER_TABLE[count][2])
		nodeport = int(FINGER_TABLE[count][3])
		while (int(count1) < int(y)):
			filekey = MY_FILE([count][0])
			if (int(nodeentry) <= int(filekey)):
				givekey = MY_FILE[count][0]
				l = str(givekey)
				givefile = MY_FILE[count][1]
				l1 = str(l)+" "+str(givefile)
				total = total+" "+str(l1)
				count1 = count1 + 1
			else:
				count1 = count1 + 1	
		count = count + 1
		givekeymsg = "GIVEKY"+str(total)
		r = int(len(givekeymsg))
		s = r + 4
		giveKey = str(s)+" "+str(givekeymsg)
		giveval = Client(nodeip, nodeport, giveKey)
		print "GIVE OK MESSAGE RECEIVED FROM: "+str(nodeip)+" "+str(nodeport)
		print "MESSAGE: "+str(giveval)
		count = count + 1
			
#--------------------------------------------------------------#
#-------------------START OF SERVER MODE-----------------------#
#--------------------------------------------------------------#

sok = socket.socket(socket.AF_INET, socket.SOCK_STREAM)		# CREATING SERVER SOCKET
MY_HOSTADDR = socket.gethostname()							# GETTING THE HOST NAME
MY_HOSTIP = socket.gethostbyname(MY_HOSTADDR)				# GETTING THE HOST IP
MY_PORT = int(sys.argv[1])								# USER INPUT FOR PORT NOS
MY_ADDRESS = (str(MY_HOSTIP), int(MY_PORT))					# DEFINING MY ADDRESS

try:
	sok.bind((MY_HOSTIP, MY_PORT))						# BINDING SOCKET TO THE ADDRESS
except	socket.error , msg:
	print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    	sys.exit()

print "Socket Binded Successfully..!!"

m = md5.new()
m.update(str(MY_HOSTIP))								# HASHING MY IP ADDRESS
myhash = m.hexdigest()
myHash = str(myhash[-8:])							# GET THE HEXADECIMAL VALUE FOR THE IP ADDRESS
#print str(myHash)			# For registering purpose
MY_HASH = int(myhash, 16)							# CONVERTING THE IP ADDRESS TO INTEGER
#print str(MY_HASH)			# Converted to integer

BS_HOST = "planetlab-1.cs.colostate.edu"				# BS SERVER
BS_IP = socket.gethostbyname(BS_HOST)					# BS IP
BS_PORT = 3090										# BS PORT
bs_reqmsg = "REG"+" "+str(MY_HOSTIP)+" "+str(MY_PORT)+" "+str(myHash)	# DEFINING BS REG REQUEST
r = int(len(bs_reqmsg))
s = r + 4
BS_REQMSG = "00"+str(s)+" "+str(bs_reqmsg)			
print BS_REQMSG

BS_RECVMSG = Client(BS_IP, BS_PORT, BS_REQMSG)			# CALLING THE CLIENT FUNCTION FOR UNREGISTERING
print str(BS_RECVMSG)								# PRINTING THE BS RECV MESSAGE

bits = int(raw_input("ENTER THE NOS OF BITS FOR KEY SPACE (max #5): "))	# USER INPUT FOR NOS OF NODES TO BE PRESENT
totalnodes = int(2**(bits))
myVal = int(MY_HASH % totalnodes)						# VALUE FOR THE CURRENT NODE

#----------------------------------------------------#
#---------------ANALYSIS OF BOOTSTRAP----------------#
#----------------------------------------------------#

SPLIT = split_nodemsg(BS_RECVMSG);			# SPLITTING THE ERROR MESSAGE

if (SPLIT == "9999"):					# ERROR FOR NODE ALREADY REGISTERED
	print "NODE ALREADY REGISTERED..!! UNREGISTER FIRST..!!"
	unreg_msg = "UNREG"+" "+str(myHash)		# DEFINING UNREGISTER STATEMENT
	r = int(len(unreg_msg))
	s = r + 4
	UNREG_MSG = "00"+str(s)+" "+str(unreg_msg)
	val = UNREG(UNREG_MSG, BS_IP, BS_PORT)		# CALLING THE CLIENT FUNCTION FOR UNREGISTER PURPOSE
	sys.exit(0)

elif (SPLIT == "0"):					# FOR FIRST NODE TO JOIN THE NETWORK
	print "REGISTER SUCCESSFUL..!!"
	print "NO  NODES IN THE NETWORK..!!"
	cond = "0"
	val = FINGERTABLE(MY_HOSTIP, MY_PORT, MY_HASH, bits);		# BUILDING ITS OWN FINGER TABLE
	val1 = myFile(bits);								# CREATING ITS OWN MY_FILE.txt
	
elif (SPLIT == "9998"):						# ERROR STATEMENT FOR ERROR IN COMMAND WHILE REGISTERING
	print "FAILED..!! ERROR IN COMMAND..!!"
	sys.exit(0)
	
elif (SPLIT == "9997"):						# ERROR STATEMENT IF BS IS FULL
	print "FAILED..!! CAN'T REGISTER BS FULL..!!"
	sys.exit(0)
	
elif (SPLIT > "0"):							# IF NODES HAVE ALREADY REGISTERED IN THE NETWORK
	print str(SPLIT)+" "+"NODES ARE PRESENT IN THE NETWORK..!!"
	n = int(SPLIT)
	cond = 0
	val = FINGERTABLE(MY_HOSTIP, MY_PORT, MY_HASH, bits);	# BUILDING ITS OWN FINGER TABLE
	val3 = myFile(bits);					# CRAETING MY_FILE.txt
	val = split_msg(BS_RECVMSG);	
	val2 = UPFIN(n);						# SENDING UPFIN COMMAND TO THE NODES ALREADY REGISTERED
	val4 = getKey(myHash, bits);				# INITIATING GETKY COMMAND
	if (str(val4) == "1"):
		val5 = addkeydef()					# INITIATING ADDKY COMMAND
	else:
		print "NO FILES TO ADD..!!"			# IF NO FILES ARE THERE TO ADD
	
else:
	print "UNKNOWN COMMAND..!!"
	

#---------------------------------------------------#
#----------------START OF SERVER MODE---------------#
#---------------------------------------------------#
sok.listen(10)									# MAKING THE SERVER SOCKET LISTN FOR 10 INCOMING CONNECTIONS

while (1):
	DATA, ADDR = sok.accept()					# ACCEPT CONNECTIONS
	MSG = DATA.recv(BUFF)						# READ MESSAGE
	#print "--------------------------------------------"
	#print str(MSG)
	words3 = MSG.split()
	WORD =  words3[1]	
	if (str(WORD) == "UPFIN"):					# UPFIN COMMAND RECEIVED
		print "UPFIN COMMAND RECEIVED FROM.."+str(ADDR)
		ipAddr = ADDR[0]
		portAddr = ADDR[1]
		print "MESSAGE: "+str(MSG)
		VAL = words3[2]
		if (VAL == "0"):						# FOR NODE JOINING THE NETWORK
			NODE_IP = words3[3]					# IP ADDRESS OF THE NODE JOINING NETWORK
			NODE_PORT = words3[4]				# PORT ADDRESS OF THE NODE JOINING THE NETWORK
			NODE_KEY = words3[5]				# KEY OF THE NODE JOINING THE NETWORK
			cond = 0
			print "NODE: "+str(NODE_IP)+" "+"PORT: "+str(NODE_PORT)+" "+"KEY: "+str(NODE_KEY)+" "+"JOINING THE NETWORK..!!"
			val = updateFingertable(NODE_IP, NODE_PORT, NODE_KEY, bits, myHash, cond);	# UPADATING FINGER TABLE FOR THE INCOMING NODE
			if (str(val) == "0"):
				print "SENDING UPFINOK MESSAGE TO: "+str(NODE_IP)+" "+str(NODE_PORT)
				UPFINOK = "0014"+" "+"UPFINOK"+" "+str(myHash)+" "+"0"	# DEFINING UPFIN OK MESSAGE
				#print str(UPFINOK)
				DATA.send(UPFINOK)				# SENDING UPFIN OK MESSAGE
				
			elif (str(val) != "0"):
				print "NODE KEY NOT RELEVANT..!!FINGER_TABLE NOT UPDATED..!!"
				upfinok = "UPFINOK"+" "+str(myHash)+" "+"9998"	# ERROR IN UPDATING THE UPFIN
				r = int(len(upfinok))
				s = r + 4
				UPFINOK = "00"+str(s)+" "+str(upfinok)
				DATA.send(UPFINOK)				# SENDING THE ERROR MESSAGE
							
			else:
				print "ERROR OCCURED WHILE UPDATING FINGER TABLE..!!"
				print "ABORTING..!!"
				sys.exit(0)
				
		elif (VAL == "1"):						# FOR THE NODE LEAVING THE NETWORK
			NODE_IP = words3[3]					# IP DECLARATION 
			NODE_PORT = words3[4]				# PORT DECLARATION
			NODE_KEY = words3[5]				# KEY DECLARATION
			
			m1 = md5.new()
			m1.update(str(NODE_KEY))
			myhash = m1.hexdigest()
			#print str(myhash)
			myHash = str(myhash[-8:])
			#print str(myHash)			# For registering purpose
			MY_HASH = int(myhash, 16)
			#print str(MY_HASH)			# Converted to integer
			VAL = int(MY_HASH % totalnode)
			print "NODE IP: "+str(NODE_IP)+" "+"PORT: "+str(NODE_PORT)+" "+"KEY: "+str(NODE_KEY)
			print "UPDATING ROUTING TABLE..!!"			# UPDATING ROUTING TABLE FOR THE NODE LEAVING THE NETWORK
			cond = 1
			val = updateFingertable(NODE_IP, NODE_PORT, NODE_KEY, bits, MY_HASH, cond);
			if (str(val) == "0"):
				print "SENDING UPFINOK MESSAGE TO: "+str(NODE_IP)+" "+str(NODE_PORT)
				UPFINOK = "0014"+" "+"UPFINOK"+" "+"9997"
				print "---------------------------------------------------------"
				DATA.send(UPFINOK)
			else:
				print "ERROR OCCURED WHILE UPDATING FINGER TABLE..!!"
				print "ABORTING..!!"
				#sys.exit(0)
					
							
	elif (str(WORD) == "GETKY"):						# GETKY COMMAND RECEIVED
		print "GETKEY COMMAND RECEIVED FROM: "+str(ADDR)		
		print "MESSAGE: "+str(MSG)
		words = MSG.split()
		#print "KEY RECEIVED: "+str(words[2])
		recvkey = str(words[2])
		fileList = getkeyok(recvkey, myHash, bits, ipAddr, portAddr);	# CALLING THE GETKYOK FUNCTION
		if (str(fileList) == " "):
			getkyokmsg = "GETKYOK"+" "+"9997"						# DEFINING ERROR MESSAGE FOR GETKYOK
			r = int(len(getkyokmsg))
			s = r + 4
			getkyMsg = "0"+str(s)+" "+str(getkyokmsg)				
			print "SENDING GETKYOK MSG TO: "+str(ADDR)
			#print str(getkyMsg)
			DATA.send(getkyMsg)								# SENDING THE GETKYOK MSG
		else:
			totalstr = str(fileList)							# GETKY STATEMENT
			getkyokmsg = "GETKYOK"+str(totalstr)
			r = int(len(getkyokmsg))
			s = r + 4
			getkyMsg = "0"+str(s)+" "+str(getkyokmsg)
			print "SENDING GETKYOK MSG TO: "+str(ADDR)			# SENDING THE GETKY MSG
			#print str(getkyMsg)
			DATA.send(getkyMsg)								# SENDING THE GETKY OK
		
	elif (str(WORD) == "GIVEKY"):				# GIVEKY COMMAND RECEIVED
		print "------------------------------------------------------------------"
		print "GIVEKY COMMAND RECEIVED FROM: !!"+str(ADDR)
		print "MESSAGE: "+str(MSG)
		words = MSG.split()
		print "ADDING FILES TO MY_LIST"
		print "UPDATED MY_FILE.txt"
		for i in MY_FILE:					# ADDING FILES TO MY_FILE.txt
			print i
		givekyval = "0015"+" "+"GIVEKYOK"+" "+"0"	# DEFINING GIVEKYOK MSG
		DATA.send(givekyval)					# SENDING THE GIVEKYOK MSG
		
	
	elif (str(WORD) == "SER"):					# SEARCH COMMAND RECEIVED
		print "-------------------------------------------------------------------"
		print "SEARCH COMMAND RECEIVED FROM: "+str(ADDR)
		print "MESSAGE: "+str(MSG)
		words = MSG.split()
		serCount  = serCount + 1					# INCREASING THE SEARCH COUNT, TO KEEP TRACK OF HOW MANY SEARCH MESSAGES RECEIVED
		print "SEARCH COUNT TILL NOW: "+str(serCount)
		QUERYIP = words[2]					# IP ADDRESS OF THE NODE WHICH SEND THE SEARCH QUERY
		QUERYPORT = words[3]				# PORT OF THE NODE WHICH SEND THE QUERY NODE
		filename = str(words[4])				# FILENAME TO BE SEARCHED
		HOPS = words[5]
		m = md5.new()
		m.update(str(filename))
		filehash = m.hexdigest()
		fileHash = filehash[-8:]
		FILE_HASH = int(fileHash, 16)
		fileval = int(FILE_HASH % 32)			# VALUE OF THE FILE WHICH IS TO BE SEARCHED
		#print str(HOPS)
		if (str(HOPS) <= "5"):				# CHECKING IF THE HOP COUNT EXCEEDED 5
			x = int(len(MY_FILE))
			count = 0
			nos = 0
			while (int(x) < int(count)):		# SEARCHING WHETHER THE CURRENT NODE HAS THAT FILE
				if (str(MY_FILE[count][0]) == str(fileval)):
					nos = nos + 1			# NOS OF FILES IN THE NODE
					count = count + 1
				else:
					count = count + 1
			if (int(nos) != 0):				# IF THE NODE DOES'NT HAVE THE FILE
				print "SEARCH SUCCESSFUL FOR: "+str(filename)
				hops = int(HOPS)
				hops = hops + 1
				newhops = hops
				serok_msg = "SEROK"+" "+str(nos)+" "+str(MY_HOSTIP)+" "+str(MY_PORT)+" "+str(filename)+" "+str(newhops)	# DEFINING SEROK
				r = int(len(serok_msg))
				s = r + 4
				SEROK_MSG = "00"+str(s)+" "+str(serok_msg)	# DEFINING SEROK MSG
				serval = DATA.send(SEROK_MSG)		# SENDING THE SEROK MSG
				DATA.close()
			
			else:
				print "KEY NOT PRESENT IN CURRENT NODE..!!"
				print "FORWARDING SEARCH KEY..!!"
				serval = search(filename, QUERYIP, QUERYPORT, HOPS, MSG)	# FORARDING THE SEARCH MESSAGE TO OTHER NODES
				DATA.send(serval)						# SENDING THE SEARCH MESSAGE 
				DATA.close()
			
		else:
			print "HOP COUNT EXCEEDED..!!"+str(HOPS)		# HOP COUNT EXCEEDS IN THE SEARCH 
			print "KEY NOT SEARCHED..!!"
			sermsg = "0098"+" "+"SEROK"+" "+"9998"			# SENDING THE SEARCH OK COMMAND
			DATA.send(sermsg)
			DATA.close()
			
	elif (str(WORD) == "ADD"):						# ADD COMMAND RECEIVED
		print "\n"
		print "----------------------------------------------------"
		print "ADD COMMAND RECEIVED FROM: "+str(ADDR)
		print "MESSAGE: "+str(MSG)
		adddefval = addkeynode(MSG)
		if (str(adddefval) == "0"):
			addokmsg = "ADDOK"+" "+"0"					# DEFINING ADDOK MESSAGE
			r = int(len(addokmsg))
			s = r + 4
			addokMsg = "00"+str(s)+" "+str(addokmsg)
			DATA.send(addokMsg)							# SENDING ADD OK FUNCTION
		
		else:
			print "ERROR OCCURRED IN ADD RESULT..!!"
			addokmsg = "ADDOK"+" "+"9997"					# DEFINING ADDOK MESSAGE IN CASE OF ERROR
			r = int(len(addokmsg))
			s = r + 4
			addokMsg = "00"+str(s)+" "+str(addokmsg)
			DATA.send(addokMsg)							# SENDING ADDOK MESSAGE
		
	elif (str(WORD) == "LEAVE"):							# LEAVE COMMAND RECEIVED
		print "-------------------------------------------------------"
		print "LEAVE COMMAND RECEIVED FROM: "+str(ADDR)
		print "MESSAGE: "+str(MSG)
		leaveok = leavenode(myHash)						# CALLING THE LEAVE OK FUNCTION
		if (str(leaveok) == "0"):
			print "LEAVE COMMAND EXECUTED SUCCESSFULLY..!!"
			setflag = 1 
		else:
			setflag = 0
		givereply = givekey(myHash)						# CALLING THE GIVEKEY FUNCTION
		print "MESSAGE RECEIVED AFTER GIVEKEY COMMAND: "
		print str(givereply)
		words = givereply.split()
		if (str(words) == "GIVEKYOK"):
			leaveok = "0015"+" "+"LEAVEOK"+" "+"0"			# DEFINING LEAVEOK TO THE CONTROLLER NODE
			leaveokmsg = str(leaveok)
			DATA.send(leaveokmsg)						# SENDING LEAVE OK TO THE CONTROLLER NODE
		else:
			print "UNKNOWN COMMAND FOUND IN GIVEOK..!!"
		unregmsg = "0014"+" "+"UNREG"+" "+str(myHash)		# DEFINING UNREGISTER COMMAND
		unregval = UNREG(unregmsg, BS_IP, BS_PORT)			# UNREGISTERING FROM THE BS
		sys.exit(0)
	else:
		print "UNKNOWN COMMAND RECEIVED FROM: "+str(ADDR)		# UNKNOWN COMMAND
		print "MESSAGE: "+str(MSG)
	
		
				
		
		
		 
		
		


